<?php

namespace App\Controller;

use App\Service\UrlService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Url;

class CompareController extends AbstractController
{
    protected $urlService;

    public function __construct (UrlService $urlService)
    {
        $this->urlService = $urlService;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('url', TextType::class, [
                'constraints' => [new Url(
                    [
                        'protocols' => ['http', 'https'],
                        'message'   => 'Url needs to have http or https like: http://tryToCheck.com'
                    ]
                )]
            ])
            ->add('urlCheck', TextType::class, [
                'constraints' => [new Url(
                    [
                        'protocols' => ['http', 'https'],
                        'message'   => 'Url needs to have http or https like: http://tryToCheck.com'
                    ]
                )]
            ])
            ->add('email', EmailType::class)
            ->add('submit', SubmitType::class, ['label' => 'Porównaj'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataForm   = $form->getData();
            $compare    = $this->urlService->execute($dataForm['url'], $dataForm['urlCheck'], $dataForm['email']);

            if($compare) {
                $text = 'Information was sent on email';
            } else {
                $text = 'Bad data into form';
            }
            return $this->render('confirmation.html.twig', [
                'text'    => $text,
            ]);
        }

        return $this->render('base.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
