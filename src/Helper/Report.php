<?php
namespace App\Helper;

class Report
{
    public function execute(string $url, float $urlTime, string $urlCheck, float $urlCheckTime) : array
    {
        $data = [
            'url' => [
                'time'  => $urlTime,
                'url'   => $url
            ],
            'urlCheck' => [
                'time' => $urlCheckTime,
                'url'  => $urlCheck
            ]
        ];

        return $data;
    }
}