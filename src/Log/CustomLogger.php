<?php
namespace App\Log;

use Psr\Log\LoggerInterface;

class CustomLogger
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function info($message)
    {
        $this->logger->info($message);
    }
}