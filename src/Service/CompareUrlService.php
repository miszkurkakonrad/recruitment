<?php
namespace App\Service;

class CompareUrlService
{
    public function execute(string $url, string $urlCheck)
    {
        $regexUrl       = preg_match("/^https?:\/\/(.*)/", $url);
        $regexUrlCheck  = preg_match("/^https?:\/\/(.*)/", $urlCheck);

        if($regexUrl == true and $regexUrlCheck == true) {
            return true;
        }

        return false;
    }
}