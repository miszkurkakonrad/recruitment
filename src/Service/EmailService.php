<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * Class EmailService
 *
 * @package App\Service
 */
class EmailService extends AbstractController
{

    public function sendEmail(string $email, array $data,  \Swift_Mailer $mailer)
    {

            $message = (new \Swift_Message('Information email'))
                ->setFrom('from@email.com')
                ->setTo($email)
                ->setBody(
                    $this->renderView(
                        'emails/information.html.twig',
                        array('data' => $data)
                    ),
                    'text/html'
                );

            $mailer->send($message);

            return true;
    }
}