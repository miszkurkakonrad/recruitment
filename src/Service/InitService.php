<?php
namespace App\Service;

class InitService
{
    public function execute(string $url) : float
    {
        $info['total_time'] = 0;
        $ch                 = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        if(curl_exec($ch))
        {
            $info = curl_getinfo($ch);
        }

        curl_close($ch);

        return $info['total_time'];
    }
}