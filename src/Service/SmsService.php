<?php

namespace App\Service;

use Headsnet\Sms\SmsSendingInterface;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * Class SmsService
 *
 * @package App\Service
 */
class SmsService
{
    public function sendSms()
    {
        $message = new \Esendex\Model\DispatchMessage(
            "WebApp", // Send from
            "01234567890", // Send to any valid number
            "My Web App is SMS enabled!",
            \Esendex\Model\Message::SmsType
        );
        $authentication = new \Esendex\Authentication\LoginAuthentication(
            "EX000000", // Your Esendex Account Reference
            "user@example.com", // Your login email address
            "password" // Your password
        );
        $service = new \Esendex\DispatchService($authentication);
        $result = $service->send($message);
        print $result->id();
        print $result->uri();
    }
}