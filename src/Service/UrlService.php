<?php

namespace App\Service;

use App\Log\CustomLogger;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\Config\Definition\Exception\Exception;


/**
 * Class UrlService
 *
 * @package App\Service
 */
class UrlService
{
    private $mailer;
    private $logger;
    private $container;

    public function __construct (ContainerInterface $container, \Swift_Mailer $mailer, CustomLogger $logger)
    {
        $this->container    = $container;
        $this->mailer       = $mailer;
        $this->logger       = $logger;
    }

    public function execute(string $url, string $urlCheck, string $email) {
        $emailService   = $this->container->get('my.service.email');
        $smsService     = $this->container->get('my.service.sms');
        $helperReport   = $this->container->get('helper.report');
        $initPage       = $this->container->get('init.page');
        $compareUrl     = $this->container->get('compareUrl.service');
        $urlTime        = $initPage->execute($url);
        $urlCheckTime   = $initPage->execute($urlCheck);

        if($compareUrl->execute($url, $urlCheck)) {
            $this->logger->info('Time for url ' . $url . 'is ' . $urlTime);
            $this->logger->info('Time for url ' . $urlCheck . 'is ' . $urlCheckTime);

            $diff = $this->diff($urlTime, $urlCheckTime);

            if($diff < 1) {
                try {
                    $emailService->sendEmail($email, $helperReport->execute($url, $urlCheckTime, $urlCheck, $urlCheckTime), $this->mailer);
                    return true;
                } catch (\Exception $exception) {
                    return $exception->getMessage();
                }
            } elseif ($diff > 1 ) {
                try {
                    $emailService->sendEmail($email, $helperReport->execute($url, $urlCheckTime, $urlCheck, $urlCheckTime), $this->mailer);
                    $smsService->sendSms();
                    return true;
                } catch (\Exception $exception) {
                    return $exception->getMessage();
                }
            } else {
                return false;
            }
        }

        return false;
    }

    public function diff($v1, $v2) : int
    {
        return ($v1-$v2) < 0 ? (-1)*($v1-$v2) : ($v1-$v2);
    }
}