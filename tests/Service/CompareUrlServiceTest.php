<?php
namespace App\Tests\Util;

use App\Controller\CompareController;
use App\Service\CompareUrlService;
use App\Service\UrlService;
use PHPUnit\Framework\Test;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SecurityBundle\Tests\Functional\app\AppKernel;
use Symfony\Bundle\TwigBundle\Tests\TestCase;

class CompareUrlServiceTest extends WebTestCase
{
    public function testExecute_correctParameter()
    {
        $serv   = new CompareUrlService();

        $result = $serv->execute('http://www.onet.pl', 'http://www.wp.pl');
        $this->assertEquals(1, $result);

    }

    public function testExecute_unCorrectParameter()
    {
        $serv = new CompareUrlService();

        $result = $serv->execute('asdasd', 'asdasd');
        $this->assertEquals(0, $result);

    }

}