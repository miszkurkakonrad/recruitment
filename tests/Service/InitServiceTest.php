<?php
namespace App\Tests\Util;

use App\Controller\CompareController;
use App\Service\InitService;
use App\Service\UrlService;
use PHPUnit\Framework\Test;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SecurityBundle\Tests\Functional\app\AppKernel;
use Symfony\Bundle\TwigBundle\Tests\TestCase;

class InitServiceTest extends TestCase
{
    public function testExecute_correctParameter()
    {
        $serv   = new InitService();

        $result = $serv->execute('http://www.onet.pl');
        $this->assertTrue(is_float($result));

    }

    public function testExecute_unCorrectParameter()
    {
        $serv   = new InitService();

        $result = $serv->execute('123123123');
        $this->assertEquals(0, $result);

    }

}